public class MainPila {

	public static void main(String[] args) {
	//float decimal= 1.0f; para que el numero decimal sea contemplado como un numero en coma flotante

       pila p;
       p = new pila();
       p.poner(6.0);
       p.poner(5.0);
       p.poner(4.0);
       p.poner(3.0);
       p.poner(2.0);

       //cout << p.sacar()<< endl;
       System.out.println(p.sacar());
       System.out.println(p.sacar());
       System.out.println(p.sacar());
       System.out.println(p.sacar());
   	}
  }

    	class pila //una pila en el cual el último en entrar es el primero en salir
         {
           public static final int max= 100;
           private int cima;
           //double a[max]; En java no se puede definir directamente el tamaño del arreglo se hace como en el apartado sig
           private double[] a = new double [max];

           public pila(){
           cima = 0;
           }

           public double sacar()
           {
           if(cima <= 0){
           System.out.print(" Error pila vacia");
           return 0.0;
           }
           else
           return a[--cima];
           }

           public void poner(double x)
           {
           if(cima >= max)
           System.out.print(" Error pila llena ");
           //cout << " error pila llena";
           else
           a[cima++]= x;
            }

        }
