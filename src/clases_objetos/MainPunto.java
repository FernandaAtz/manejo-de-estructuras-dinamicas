
public class MainPunto {
	public static void main(String[] args) {
	//float decimal= 1.0f; para que el numero decimal sea contemplado como un numero en coma flotante

    Punto p, p1; // p, y p1 son objetos de la clase Punto
    p = new Punto();
    p1 = new Punto();
    Punto p2 = new Punto(5.0, 6.0, 7.0); //p2 objeto de la clase Punto
    Punto p3 = new Punto(8.0); //p3 objeto de la clase Punto
     p.AsignarPunto(2.0,3.0,4.0); // llamada a función miembro;
    System.out.println(p);
    System.out.println(p1);
    System.out.println(p2);
    System.out.println(p3);

  /*  p.EscribirPunto(); // llamada a función miembro
    p1.EscribirPunto(); //Escribe valores constructor por defecto
    p2.EscribirPunto(); //Escribe valores constructor alternativo
    p3.EscribirPunto(); //Escribe valores constructor lista
   // system("PAUSE"); no existe en java
   // return EXIT_SUCCESS; " " */
	}
}

       class Punto // nombre de la clase
       {
        // zona pública con declaración de funciones miembro
       public double ObtenerX() { return x; } //selector x
       public double ObtenerY() { return y;} //selector y
       public double ObtenerZ() { return z;} //selector z
       public void PonerX (double valx) { x = valx; } //modificador x
       public void PonerY (double valy) { y = valy; } //modificador y
       public void PonerZ (double valz) { z = valz;} //modificador z

       public  Punto(double valx) { //constructor
       x=valx;
       y=0.0;
       z=0.0;
       }
       // constructor que inicializa miembros Las declaraciones anteriores son definiciones de métodos, ya que
       //incluyen el cuerpo de cada función. Son funciones en línea, inline

       Punto() //constructor por defecto
       { //cuerpo constructor por defecto
       x = 0;
       y = 0;
       z = 0;
       }

       Punto(double valx, double valy, double valz) //constructor alternativo
       { //cuerpo constructor alternativo
       x = valx;
       y = valy;
       z = valz;
       }

       // Punto( const Punto &p); // constructor de copia
       Punto ( Punto p) // constructor de copia
       { // cuerpo del constructor de copia
       x = p.x;
       y = p.y;
       z = p.z;
       }

        @Override
        public String toString(){
        String nuevaPinta = " " + x + " " + y + " " +  z ;
        return nuevaPinta;
        }

//Las declaraciones anteriores no incluyen el cuerpo de cada función son funciones fuera de línea.El cuerpo de la función se declara independientemente
       void AsignarPunto(double valx, double valy, double valz) // modificador
       { //Cuerpo. El nombre de los parámetros puede omitirse en declaración
       x = valx;
       y = valy;
       z = valz;
       }


       // zona privada con declaración de atribitos.
       private double x, y, z;
       }





