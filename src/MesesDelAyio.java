//Declarar un tipo de dato enumer ado para representar los meses del año, el mes enero debe de estar asociado al dato entero 1, y así sucesivamente los demás meses.

import java.util.Scanner;
public class MesesDelAyio {
    static Scanner sc = new Scanner(System.in);

    enum LosMesesDelAyio { ENERO, FEBREO, MARZO, ABRIL, MAYO, JUNIO, JULIO, AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE };

	public static void main(String[] args) {
        int opcion;
        LosMesesDelAyio meses;

            do
            {
                System.out.println("  MESES DEL AÑO ");
                System.out.println("1.- ENERO ");
                System.out.println("2.- FEBREO ");
                System.out.println("3.- MARZO ");
                System.out.println("4.- ABRIL ");
                System.out.println("5.- MAYO ");
                System.out.println("6.- JUNIO ");
                System.out.println("7.- JULIO ");
                System.out.println("8.- AGOSTO ");
                System.out.println("9.- SEPTIEMBRE ");
                System.out.println("10.- OCTUBRE");
                System.out.println("11.- NOVIEMBRE ");
                System.out.println("12.- DICIEMBRE ");

                System.out.println("Escoja una mes del año: ");
                opcion =sc.nextInt();
                meses=determinarMes(opcion);
          //      visualizar ( mes );

                } while (meses != null);
       }

       public static LosMesesDelAyio determinarMes(int opc) {
            switch (opc){
            case 1:
            return LosMesesDelAyio.ENERO;

            case 2:
            return LosMesesDelAyio.FEBREO;

            case 3:
            return LosMesesDelAyio.MARZO;

            case 4:
            return LosMesesDelAyio.ABRIL;

            case 5:
            return LosMesesDelAyio.MAYO;

            case 6:
            return LosMesesDelAyio.JUNIO;

            case 7:
            return LosMesesDelAyio.JULIO;

            case 8:
            return LosMesesDelAyio.AGOSTO;

            case 9:
            return LosMesesDelAyio.SEPTIEMBRE;

            case 10:
            return LosMesesDelAyio.OCTUBRE;

            case 11:
            return LosMesesDelAyio.NOVIEMBRE;

            case 12:
            return LosMesesDelAyio.DICIEMBRE;
            default:
            return null;
        }
  }  }
