import java.util.Scanner;
public class EstacionesAnyo {
    static Scanner sc = new Scanner(System.in);
    enum Estaciones { PRIMAVERA, VERANO, OTOyO, INVIERNO };

	public static void main(String[] args) {
        int opcion;
        Estaciones estacion;

            do
            {
                System.out.println("  ESTACIONES DEL AÑO ");
                System.out.println("1.- PRIMAVERA: ");
                System.out.println("2.- VERANO: ");
                System.out.println("3.- INVIERNO: ");
                System.out.println("4.- OTOÑO: ");
                System.out.println("Escoja una estacion del año: ");
                opcion =sc.nextInt();
                estacion=determinarEstacion(opcion);
                visualizar ( estacion );

                } while (estacion != null);
       }

       public static Estaciones determinarEstacion(int opc) {
            switch (opc){
            case 1:
            return Estaciones.PRIMAVERA;

            case 2:
            return Estaciones.VERANO;

            case 3:
            return Estaciones.INVIERNO;

            case 4:
            return Estaciones.OTOyO;
            default:
            return null;
        }
    }

    //Escribir una función que reciba el tipo enumerado del Ejercicio 8.1 y lo visualice.
    public static void visualizar (Estaciones estacion) {
            if (estacion==null)
            return;

            switch (estacion){
            case PRIMAVERA:
            System.out.println("PRIMAVERA ");
            break;

            case VERANO:
            System.out.println("VERANO ");
            break;

            case INVIERNO:
            System.out.println("INVIERNO");
            break;

            case OTOyO:
            System.out.println("OTOÑO ");
            break;
            default:
            System.out.println("Estacion invalida");
        }
    }
}

  //  enum Estaciones { PRIMAVERA, VERANO, OTOyO, INVIERNO } SE PUEDE PONER DENTRO O FUERA DE CLASE

