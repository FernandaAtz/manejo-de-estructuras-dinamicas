public class MainCoordenadas {
	public static void main(String[] args) {
        Punto p1, p2, p3;
        p1= new Punto();
        p2= new Punto();
        p3= new Punto();
        p1.x = 1.0; p1.y = 2.0; p1.z = 3.0;
        p2.x = 8.0; p2.y = 9.0; p2.z = 10.0;
        p3.x = 0.0; p3.y = 0.0; p3.z = 0.0;
        System.out.println(" Antes: ");
        System.out.println("p3.x:  "+ p3.x);
        System.out.println("p3.y: "+ p3.y);
        System.out.println("p3.z: " + p3.z);
        p3.sumar (p1, p2);
        System.out.println(" Despues: ");
        System.out.println("p3.x: " + p3.x );
        System.out.println("p3.y: " + p3.y  );
        System.out.println("p3.z: " + p3.z  );
	}
}

class Punto{                                              
// miembros dato
double x, y, z;
// funciones miembro
void sumar (Punto p1,  Punto p2) {
    
x = p1.x + p2.x;
y = p1.y + p2.y;
z = p1.z + p2.z;
}

void Restar ( Punto p1, Punto p2)
{
x = p1.x - p2.x;
y = p1.y - p2.y;
z = p1.z - p2.z;
}
}
