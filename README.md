# UNIDAD II: Manejo de estructuras dinámicas implementadas con programación orientada a objetos.

## OBJETIVOS PARTICULARES DE LA UNIDAD

El alumno diseñará e implementará un programa para la resolución de problemas de ingeniería empleando
estructuras de datos.

| No. Tema  | Título Tema                      | Teoría              | Práctica              | Evaluación Continua           | Clave Bibliográfica  |
| :---      | :----                            | :---:               | :---:                 | :---:                         | :---:                |
| 2         | TIPOS DE ESTRUCTURAS DE DATOS    | :white_check_mark:  | :white_check_mark:    | :white_check_mark:            | [2B, 3B](activos/pdfs/UNIDAD_II-Estructuras_y_uniones.pdf)               |
| 2.1       | Struct                           | :white_check_mark:  | :x:    | :white_check_mark: |  [2B, 3B](activos/pdfs/UNIDAD_II-Estructuras_y_uniones.pdf)               |
| 2.1.1     | Unión                            | :white_check_mark:  | :x:    | :white_check_mark: |  [2B, 3B](activos/pdfs/UNIDAD_II-Estructuras_y_uniones.pdf)               |
| 2.2       | APUNTADORES A ESTRUCTURAS        | :white_check_mark:  | :white_check_mark:    | :white_check_mark: |  [2B, 3B](activos/pdfs/UNIDAD_II-Apuntadores.pdf)    |
| 2.3       | ASIGNACION DINAMICA A ESTRUCTURAS| :white_check_mark:  | :white_check_mark:    | :white_check_mark: |  [2B, 3B](activos/pdfs/UNIDAD_II-Estructuras_y_uniones.pdf)      |
| 2.4       | CLASES                           | :white_check_mark:  | :white_check_mark:    | :white_check_mark: |  [2B, 3B](activos/pdfs/UNIDAD_II-Clases_y_objetos.pdf)         |
|           | Horas Totales                    | 6.0                 | 4.5                   | 6.0                           |         |

## ESTRATEGIA DIDÁCTICA

El alumno resolverá ejercicios aplicando los diferentes tipos de estructuras de datos y discusión de las diferencias
existentes.

## PROCEDIMIENTO DE EVALUACIÓN

* Programas y ejercicios desarrollados en clase y extra clase.
* Examen del periodo.
